from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from hardpointeditorui import Ui_Form_hardpointEditor
from models import HardPointTableModel

class HardPointEditor(QDialog, Ui_Form_hardpointEditor):

    """
    the generic array tab handler

    """

    def __init__(self, str_data, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(HardPointEditor, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.Data = str_data
        self.SaveOnExit = False
        self.pushButton_Done.pressed.connect(self.donePressed)

        self.dataModel = HardPointTableModel(str_data)
        mchList = self.dataModel.getMechList()
        self.comboBox.addItems(mchList)
        self.comboBox.setCurrentIndex(0)
        self.comboBox.currentIndexChanged.connect(self.updateTable)
        self.updateTable()

        # set up the inventory table
        self.tableView.setModel(self.dataModel)
        self.tableView.verticalHeader().show()
        self.tableView.horizontalHeader().setResizeMode(0, QHeaderView.Stretch)
        self.tableView.setSelectionMode(QAbstractItemView.SingleSelection)



    def updateTable(self):

        self.dataModel.update_table(self.comboBox.currentIndex())

    def donePressed(self):
        self.Data = self.dataModel.finialize()
        self.SaveOnExit = True
        self.close()