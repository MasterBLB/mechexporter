
class MechBuildErrors(object):

    Err_Success = 0
    Err_NoSpace = 1
    Err_InvalidMechDef = 2
    Err_InvalidChassisDef = 3
    Err_Unknown = 4
    Err_MissingRecord = 5