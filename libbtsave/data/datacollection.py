from ..encoders import IntEncoder, encoder_factory
from .datarecords import DataRecord
from .transactionlog import TransactionLog
from ..utils import DataType

class DataCollection(object):
    """
    a representation of the games stat collection holding various records

    """

    def __init__(self):
        self.id = 0
        self.subIdCounter = 0
        self.recordCount = 0
        self.transactionCount = 0
        self._records = []
        self._transactionLogs = []
        self._undecodedtransactions = ''


    def fromBytes(self, lst_bytes, int_offset):
        """
        load the data from its serialized form

        :param lst_bytes: the data as a list of bytes in integer form
        :type lst_bytes: list[int]
        :param int_offset: the offset in the list to start from
        :type int_offset: int
        :return: the new offset
        :rtype: int
        """
        self._records = []
        self._transactionLogs = []
        self._undecodedtransactions = ''

        obj_id = IntEncoder()
        int_new_offset = obj_id.fromBytes(lst_bytes, int_offset)
        self.id = obj_id.value
        int_new_offset = obj_id.fromBytes(lst_bytes, int_new_offset)
        self.subIdCounter = obj_id.value
        int_new_offset = obj_id.fromBytes(lst_bytes, int_new_offset)
        self.recordCount = obj_id.value
        for x in range(self.recordCount):
            tmp = DataRecord()
            int_new_offset = tmp.fromBytes(lst_bytes, int_new_offset)
            self._records.append(tmp)
        # ToDo: figure out if we really need to parse or add the transaction logs
        # for now just figure out how long the data is and leave it be
        int_new_offset = obj_id.fromBytes(lst_bytes, int_new_offset)
        self.transactionCount = obj_id.value
        self._undecodedtransactions = lst_bytes[int_new_offset:]
        # this is vestigal from earlier work, just leave for now, clean up later
        return len(lst_bytes)


    def toBytes(self):
        """
        re-serialize this object

        :return: a list of bytes in integer form
        :rtype: lsit[int]
        """
        obj_id = IntEncoder()
        obj_id.setValue(self.id)
        lst_data = obj_id.toBytes()
        obj_id.setValue(self.subIdCounter)
        lst_data += obj_id.toBytes()
        obj_id.setValue(self.recordCount)
        lst_data += obj_id.toBytes()
        for record in self._records:
            lst_data += record.toBytes()
        obj_id.setValue(self.transactionCount)
        lst_data += obj_id.toBytes()
        lst_data += self._undecodedtransactions
        return lst_data

    def updateRecordInt(self, str_name, int_value):
        """
        update a record, or create one if it does not exist

        :param str_name: the name of the record
        :type str_name: str
        :param int_value: the value
        :type int_value: int
        :return:
        :rtype:
        """
        record = self.findRecord(str_name)
        if record is None:
            record = DataRecord()
            record.name = str_name
            record.currentValue.key = 'System.Int32'
            record.defaultValue.key = 'System.Int32'
            record.currentValue.setValue(1)
            record.defaultValue.setValue(1)
            self.addRecord(record)
        record.currentValue.setValue(int_value)

    def updateRecordStr(self, str_name, str_value):
        """
        update a record, or create one if it does not exist

        :param str_name: the name of the record
        :type str_name: str
        :param str_value: the value
        :type str_value: str
        :return:
        :rtype:
        """
        record = self.findRecord(str_name)
        if record is None:
            record = DataRecord()
            record.name = str_name
            record.currentValue.key = 'System.String'
            record.defaultValue.key = 'System.String'
            record.currentValue.setValue("UnSet", DataType.String)
            record.defaultValue.setValue("UnSet", DataType.String)
            self.addRecord(record)
        record.currentValue.setValue(str_value, DataType.String)

    def deleteTransactionLogs(self):
        """
        delete the history logs for this record collection

        :return:
        :rtype:
        """
        self._undecodedtransactions = ''
        self.transactionCount = 0

    def findRecords(self, str_prefix):
        """
        find all records that starts with a given prefix

        :param str_prefix: the prefix
        :type str_prefix: str
        :return: all the records starting with the given prefix
        :rtype: list[DataRecord]
        """
        lst_records = []
        for record in self._records:
            if record.name.startswith(str_prefix):
                lst_records.append(record)
        return lst_records

    def findRecord(self, str_name):
        """
        find a specific record

        :param str_name: the record name
        :type str_name: str
        :return: the record if it exists
        :rtype: DataRecord
        """
        for record in self._records:
            if record.name == str_name:
                return record
        return None

    def deleteRecord(self, obj_record):
        """
        delete the record from the collection

        :param obj_record: the record to be deleted
        :type obj_record: DataRecord
        :return:
        :rtype:
        """
        if obj_record in self._records:
            self._records.remove(obj_record)
            self.recordCount -= 1

    def addRecord(self, obj_record):
        """
        add a record to the collection

        :param obj_record:
        :type obj_record: DataRecord
        :return:
        :rtype:
        """
        self.subIdCounter += 1
        self.recordCount += 1
        obj_record.uid = self.subIdCounter
        self._records.append(obj_record)

    @property
    def records(self):
        return self._records