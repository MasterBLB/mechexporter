from ..encoders import encoder_factory, StringEncoder, IntEncoder
from .keyvalpair import KeyValPair


class DataRecord(object):
    """
    representation of a record in the data collection

    """

    def __init__(self):
        self.name = ''
        self.uid = 0
        self.currentValue = KeyValPair()
        self.defaultValue = KeyValPair()

    def fromBytes(self, lst_bytes, int_offset):
        obj_id = StringEncoder()
        int_new_offset = obj_id.fromBytes(lst_bytes, int_offset)
        self.name = obj_id.value
        obj_id = IntEncoder()
        int_new_offset = obj_id.fromBytes(lst_bytes, int_new_offset)
        self.uid = obj_id.value
        int_new_offset = self.currentValue.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.defaultValue.fromBytes(lst_bytes, int_new_offset)
        return int_new_offset

    def toBytes(self):
        obj_id = StringEncoder()
        obj_id.setValue(self.name)
        lst_data = obj_id.toBytes()
        obj_id = IntEncoder()
        obj_id.setValue(self.uid)
        lst_data += obj_id.toBytes()
        lst_data += self.currentValue.toBytes()
        lst_data += self.defaultValue.toBytes()
        return lst_data