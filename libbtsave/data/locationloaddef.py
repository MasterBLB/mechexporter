from ..utils import convert_to_stringio, convert_to_bytes, convert_to_string
from ..encoders import encoder_factory, ByteArrayEncoder
from cStringIO import StringIO
from hashlib import md5
from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from ..protobuf import ProtobufMessage, EWireType
from .enummember import EnumMember
import time
import struct

class LocationLoadoutDef(object):

    """
    the save data representation of a mech in the company

    """

    def __init__(self):
        """
        init the object

        """
        self._rawData = None
        self._repackedData = None
        self.mainMessage = ProtobufMessage()
        self._location = EnumMember()
        self._status = EnumMember()

    def fromBytes(self, lst_bytes, int_offset=0):
        self._rawData = lst_bytes
        self.mainMessage.fromBytes(lst_bytes, 0)
        self._location.fromBytes(self.mainMessage.get_field(1).data)
        self._status.fromBytes(self.mainMessage.get_field(7).data)

    def toBytes(self):

        return self.mainMessage.toBytes()

    @property
    def Location(self):
        return self._location.value

    @property
    def Status(self):
        return self._status.value

    @staticmethod
    def newItem(str_location, int_armor, int_reararmor, int_internalstruct, int_assignedarmor, int_assignedrear):

        message = ProtobufMessage()

        location = EnumMember()
        location.setType(4146553359)
        location.setValue(str(str_location))
        message.add_field(EWireType.LengthDelimited, 1, lst_data=location.toBytes())
        message.add_field(EWireType.Fixed32, 2, lst_data=struct.pack('<f', float(int_armor)))
        message.add_field(EWireType.Fixed32, 3, lst_data=struct.pack('<f', float(int_reararmor)))
        message.add_field(EWireType.Fixed32, 4, lst_data=struct.pack('<f', float(int_internalstruct)))
        message.add_field(EWireType.Fixed32, 5, lst_data=struct.pack('<f', float(int_assignedarmor)))
        message.add_field(EWireType.Fixed32, 6, lst_data=struct.pack('<f', float(int_assignedrear)))
        status = EnumMember()
        status.setType(1799457993)
        status.setValue('Functional')
        message.add_field(EWireType.LengthDelimited, 7, lst_data=status.toBytes())

        locationloadout = LocationLoadoutDef()
        locationloadout.fromBytes(message.toBytes(), 0)
        return locationloadout
