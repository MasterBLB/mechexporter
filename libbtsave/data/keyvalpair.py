from ..encoders import encoder_factory, StringEncoder, encoder_size_factory, IntEncoder, ByteArrayEncoder
from ..utils import DataType


class KeyValPair(object):

    """
    a dictionary key-value pair system typically used in the save to denote a value and its type

    """

    def __init__(self):
        self.key = ''
        self._value = None

    def fromBytes(self, lst_bytes, int_offset):
        obj_id = StringEncoder()
        int_new_offset = obj_id.fromBytes(lst_bytes, int_offset)
        self.key = obj_id.value
        result = encoder_factory(lst_bytes, int_new_offset)
        self._value = result[0]
        return result[1]

    def toBytes(self):
        obj_enc = StringEncoder()
        obj_enc.setValue(self.key)
        lst_bt = obj_enc.toBytes()
        return lst_bt + self._value.toBytes()

    def calcSize(self, lst_bytes, int_offset):
        obj_id = StringEncoder()
        int_new_offset = obj_id.calcSize(lst_bytes, int_offset)
        self.key = obj_id.value
        result = encoder_size_factory(lst_bytes, int_new_offset)
        self._value = result[0]
        return result[1]

    def keyContains(self, str_data):
        """
        check if a key contains a specific value

        :param str_data: the value to look for
        :type str_data: str
        :return: True if the key contains the value, False otherwise
        :rtype: bool
        """
        if str_data in self.key:
            return True
        return False

    @property
    def value(self):
        return self._value.value

    def setValue(self, value, int_type=1):
        """
        set the value of the pair

        :param value: the new value
        :type value:
        :param int_type: the data encoder type
        :type int_type: int
        :return:
        :rtype:
        """

        # ToDo: booleans and floats may also be possible and may need to be handled in the future
        if int_type == DataType.Int:
            self._value = IntEncoder()
        elif int_type == DataType.String:
            self._value = StringEncoder()
        elif int_type == DataType.ByteArray:
            self._value = ByteArrayEncoder()
        self._value.setValue(value)