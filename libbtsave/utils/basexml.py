import xml.etree.cElementTree as ET
from xml.dom import minidom  # for pretty xml generation
import inspect
import ast
import base64
from cStringIO import StringIO
import os.path
import zipfile
import codecs


class BaseXml(object):
    """
     A basic data class that can auto-generate itself into forms useful for printing or writing to file


    """
    XmlType = 'BaseXml'  #: XML Root Rag for this object

    def _get_members(self, bol_include_props=False):
        """
        get all non-private member attributes

        :return: a list of all members that are not private, functions or properties
        :rtype: list
        """
        lst_members = inspect.getmembers(self)
        lst_filtered_members = []
        for member in lst_members:
            # print member
            if member[0].startswith('__') or member[0].startswith('_'):
                pass
            elif callable(getattr(self, member[0])):
                pass
            elif isinstance(getattr(type(self), member[0], getattr(self, member[0])), property):
                if bol_include_props:
                    lst_filtered_members.append(member)
            elif member[0] == 'XmlType':
                pass
            else:
                lst_filtered_members.append(member)
        return lst_filtered_members

    def __str__(self):
        lst_members = self._get_members()
        str_to_return = ''
        for member in lst_members:
            if member[1] is None:
                str_to_return += member[0] + ' : \n'
            else:
                str_to_return += member[0] + ' : ' + unicode(member[1]) + '\n'
        return str_to_return

    def __unicode__(self):
        lst_members = self._get_members()
        str_to_return = u''
        for member in lst_members:
            if member[1] is None:
                str_to_return += unicode(member[0]) + u' : \n'
            elif isinstance(getattr(type(self), member[0], getattr(self, member[0])), basestring):
                str_to_return += unicode(member[0]) + u' : ' + member[1] + u'\n'
            else:
                str_to_return += unicode(member[0]) + u' : ' + unicode(member[1]) + u'\n'
        return str_to_return

    def __eq__(self, other):
        """
        define a base equality operator
        :return:
        :rtype: bool
        """

        lst_members = self._get_members()
        if not hasattr(other, 'XmlType'):
            return False
        if self.XmlType != other.XmlType:
            return False
        for member in lst_members:
            if hasattr(other, member[0]):
                if getattr(other, member[0]) != member[1]:
                    break
            else:
                break
        else:
            return True
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def _toXml(self):
        """
        make an xml representation of this object

        :return: an XML representation of the object
        :rtype: XML Element
        """

        lst_members = self._get_members()
        xml_obj = ET.Element(self.XmlType)
        for member in lst_members:
            xml_tmp = ET.SubElement(xml_obj, member[0])
            if member[1] is None:
                xml_tmp.text = ''
            elif unicode(member[1]) == 'True':
                xml_tmp.text = 'true'
            elif unicode(member[1]) == 'False':
                xml_tmp.text = 'false'
            else:
                xml_tmp.text = unicode(member[1])
        return xml_obj

    @staticmethod
    def make_xml(str_xml):
        """
        create and return an ElementTree element object from a valid xml string

        :param str_xml: an xml string
        :type str_xml: str
        :return: an XML Element object
        :rtype: XML Element
        """
        el_root = None
        try:
            obj_file = StringIO(str_xml)
            obj_file.seek(0)
            el_root = ET.parse(obj_file).getroot()
        except Exception as e:
            pass
        return el_root

    def _fromXml(self, obj_xml):
        """
        populate this object from an xml representation of the object

        :param obj_xml: the xml representation, either as an ElementTree element object or as a string
        :return: True if successful, False otherwise
        :rtype: bool
        """
        el_root = None
        if isinstance(obj_xml, basestring):
            try:
                obj_file = StringIO(obj_xml)
                obj_file.seek(0)
                el_root = ET.parse(obj_file).getroot()
            except Exception as e:
                return False
        else:
            el_root = obj_xml
        if el_root.tag != self.XmlType:
            return False

        lst_members = self._get_members()
        for member in lst_members:
            xml_tmp = el_root.find(member[0])
            if xml_tmp is not None:
                try:
                    if xml_tmp.text is not None:
                        if xml_tmp.text == 'true':
                            setattr(self, member[0], True)
                        elif xml_tmp.text == 'false':
                            setattr(self, member[0], False)
                        elif xml_tmp.text.lower() == 'none':
                            setattr(self, member[0],
                                    unicode(xml_tmp.text))  # if the string is None ast will treat it as a NoneType, we don't want that
                        else:
                            setattr(self, member[0], ast.literal_eval(xml_tmp.text))
                except (ValueError, SyntaxError):
                    if xml_tmp.text is not None:
                        setattr(self, member[0], unicode(xml_tmp.text))  # for strings I guess?
                except Exception as e:
                    print str(e)
                    print xml_tmp.text

        return True

    @classmethod
    def isOfType(cls, other):
        """
        identify if an object belongs to this data type

        :param other: the object to test
        :type other: object
        :return: True if the object is of this class, False otherwise
        :rtype: bool

        .. note::
            You dont need to create an instance to call this method

        """
        if not hasattr(other, 'XmlType'):
            return False
        if cls.XmlType == other.XmlType:
            return True
        return False

    @staticmethod
    def make_pretty_xml(el_tree):
        """
        Convert the ugly xml generated by the ETree Parser to a pretty printed form

        :param el_tree: an XML object to be converted to a pretty printed xml string
        :type el_tree: XML Element
        :return: The xml in a 'pretty print' version
        :rtype: str
        """
        str_tmp = ET.tostring(el_tree, 'utf-8')
        xml_nicer = minidom.parseString(str_tmp)
        return xml_nicer.toprettyxml()

    def toXml(self):
        """
        make an xml representation of this object

        :return: an XML representation of the object
        :rtype: XML Element
        """
        return self._toXml()

    def fromXml(self, obj_xml):
        """
        populate this item from an XML representation

        :param obj_xml: the XML either as an XML element or a string
        :type obj_xml: XML | str
        :return: True if successful, False otherwise
        :rtype: bool
        """
        return self._fromXml(obj_xml)

    def toBase64(self):
        """
        create a base64 encoded string representation of this object

        :return: a base64 encoded string
        :rtype: str
        """
        return base64.standard_b64encode(ET.tostring(self.toXml(), 'utf-8'))

    def toZip(self, str_name):
        """
        create a zip file that contains the xml file of this object

        :param str_name: the name of the file to be created, with no extension
        :type str_name: str
        :return:
        :rtype:
        """
        obj_xml = self.toXml()
        obj_zip = zipfile.ZipFile(str_name + '.zip', 'w', compression=zipfile.ZIP_DEFLATED)
        obj_zip.writestr(os.path.basename(str_name) + '.xml', self.make_pretty_xml(obj_xml))
        obj_zip.close()

    def toFile(self, str_name):
        """
        write the current details to file

        :param str_name: the name of the file to be created
        :type str_name: str
        :return:
        :rtype:
        """
        str_xml = self.make_pretty_xml(self.toXml())
        m_file = codecs.open(str_name, 'wb', 'utf-8')
        m_file.write(str_xml)
        m_file.flush()
        m_file.close()

    def fromFile(self, str_name):
        """
        load data from file

        :return:
        :rtype:
        """
        m_file = open(str_name, 'rb')
        data = m_file.read()
        m_file.close()
        self.fromXml(data)