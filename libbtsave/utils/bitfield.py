from math import ceil


class BitField(object):

    def __init__(self, int_fieldsize=0):
        self._value = 0
        self.fieldSize = int_fieldsize
        self.maskField = 0
        self.signed = False

        for x in range(self.fieldSize):
            self.maskField = (self.maskField << 1) | 1

    @staticmethod
    def concat(obj_field1, obj_field2):
        """
        concatenate 2 bit fields

        :param obj_field1:
        :type obj_field1: BitField
        :param obj_field2:
        :type obj_field2: BitField
        :return:
        :rtype: BitField
        """
        int_size = obj_field1.fieldSize + obj_field2.fieldSize
        obj_new = BitField(int_size)
        int_val = (obj_field1.Value << obj_field2.fieldSize) | (obj_field2.Value)
        obj_new.setValue(int_val)
        return obj_new

    def append(self, int_value, int_bit_len):
        int_val = (self.Value << int_bit_len) | (int_value)
        self.fieldSize += int_bit_len
        for x in range(int_bit_len):
            self.maskField = (self.maskField << 1) | 1
        self.setValue(int_val)

    def prepend(self, int_value, int_bit_len):
        int_val = (int_value << self.fieldSize) | self._value
        self.fieldSize += int_bit_len
        for x in range(int_bit_len):
            self.maskField = (self.maskField << 1) | 1
        self.setValue(int_val)

    @property
    def Value(self):
        """
        get the value of the bitfield

        :return: the value of the bitfield as an int
        :rtype: int
        """
        if not self.signed:
            return self._value
        if self.isSet(self.fieldSize - 1):
            x = BitField(int_fieldsize=self.fieldSize)
            for i in range(self.fieldSize):
                x.setBit(i)
            return (self._value - x.Value) -1
        return self._value

    @property
    def maxValue(self):
        """
        get the max value this bitfield can hold

        :return:
        :rtype: int
        """
        obj_bit = BitField(self.fieldSize)
        for x in range(self.fieldSize):
            obj_bit.setBit(x)
        return obj_bit.Value

    @property
    def hex(self):
        """
        get the value of the bit field

        :return: a hex string
        :rtype: str
        """
        return '0x{0:0{1}X}'.format(self._value, (self.fieldSize / 4))

    @property
    def bin(self):
        """
        get the value of the bit field

        :return: a binary string
        :rtype: str
        """
        return '{0:0{1}b}'.format(self._value, self.fieldSize)

    def setValue(self, int_value):
        """
        set the value of the bitfield

        :param int_value: the value to set, if larger than the allotted bits, it will be truncated
        :type int_value: int
        :return:
        :rtype:
        """
        self._value = (int_value & self.maskField)

    def setFromByteList(self, lst_bytes, bol_bigend=True):
        """
        set the value of the bit field form a list of bytes

        :param lst_bytes: the byte list
        :type lst_bytes: list[int]
        :param bol_bigend: when True (default) assume big endian byte order, when false use little endian byte order
        :type bol_bigend: bool
        :return:
        :rtype:
        """
        int_val = 0
        lst_bt = lst_bytes
        if not bol_bigend:  # if not big endian, reverse the list to put into little endian
            lst_bt = list(reversed(lst_bytes))
        for bt in lst_bt:
            int_val = ((int_val << 8) | bt)
        self.setValue(int_val)

    def toByteList(self, bol_bigend=True):
        """
        get the bit field value as a list of bytes

        :param bol_bigend: when True (default) return in big endian byte order, when False use little endian
        :type bol_bigend: bool
        :return:
        :rtype: list[int]
        """
        lst_bytes = []
        for x in range(int(ceil(self.fieldSize/8.0))):
            int_val = ((self._value >> 8*x) & 0xFF)
            lst_bytes.append(int_val)
        if bol_bigend:
            lst_bytes = list(reversed(lst_bytes))

        return lst_bytes


    def isSet(self, int_bit):
        """
        get if a particular bit is set

        :param int_bit: the bit to check (LSb is 0)
        :type int_bit: int
        :return: True if the bit is set, False otherwise
        :rtype: bool
        """
        if int_bit >= self.fieldSize:
            return False
        val = (self._value >> int_bit) & 0x1
        if val == 1:
            return True
        return False

    def _modify_bit(self, int_bit, bol_set):
        """
        modify a specific bit

        :param int_bit: the bit to modify
        :type int_bit: int
        :param bol_set: True to set the bit, False to unset it
        :type bol_set: bool
        :return: True on success, False otherwise
        :rtype: bool
        """
        if int_bit >= self.fieldSize:
            return False
        mask = 1 << int_bit
        self._value &= ~mask  # unset the bit
        if bol_set:
            self._value |= mask
        return True

    def setBit(self, int_bit):
        """
        Set a specific bit

        :param int_bit: the bit to set
        :type int_bit: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        return self._modify_bit(int_bit, True)

    def unSetBit(self, int_bit):
        """
        Unset a specific bit

        :param int_bit: the bit to unset
        :type int_bit: int
        :return: True on success, False otherwise
        :rtype: bool
        """
        return self._modify_bit(int_bit, False)

    def get_bit_struct(self, int_start_bit, int_length):
        """
        get a sub-structure of this bit field

        :param int_start_bit: the starting bit of this structure
        :type int_start_bit: int
        :param int_length: the length of this structure
        :type int_length: int
        :return: a new bitfield object that contains this sub-structure
        :rtype: BitField
        """
        val = self._value >> int_start_bit
        tmp = BitField(int_fieldsize=int_length)
        tmp.setValue(val)
        return tmp


class BitField8(BitField):

    def __init__(self):
        BitField.__init__(self, int_fieldsize=8)


class BitField16(BitField):

    def __init__(self):
        BitField.__init__(self, int_fieldsize=16)


class BitField32(BitField):

    def __init__(self):
        BitField.__init__(self, int_fieldsize=32)


class BitField64(BitField):

    def __init__(self):
        BitField.__init__(self, int_fieldsize=64)


class BitField128(BitField):

    def __init__(self):
        BitField.__init__(self, int_fieldsize=128)
