UNITYPACK_LOADED = False
try:
    import unitypack
    UNITYPACK_LOADED = True
except ImportError:
    pass

import os
import os.path
import json
import json5
import traceback

class UnityAssetReader(object):


    def __init__(self, str_path):
        self.FilePath = str_path
        self.JsonFileMap = {}

    def loadJsonFiles(self):
        try:
            with open(self.FilePath, "rb") as f:
                bundle = unitypack.load(f)
                for asset in bundle.assets:
                    for id, object in asset.objects.items():
                        if object.type == "TextAsset":
                            data = object.read()
                            # print data.name
                            try:
                                datajson = json.loads(data.script)
                            except Exception as e:
                                f.seek(0)
                                try:
                                    datajson = json5.loads(data.script)
                                except:
                                    continue
                            # print 'Here!!!'
                            self.JsonFileMap[data.name] = datajson
            # print self.JsonFileMap.keys()


        except Exception as e:
            print traceback.format_exc()
            return
