from .baseencoder import BaseEncoder
from .byteencoder import ByteEncoder
from .intencoder import IntEncoder
from ..utils import DataType


class ByteArrayEncoder(BaseEncoder):
    EncodeType = DataType.ByteArray

    def fromBytes(self, lst_bytes, int_offset):
        self.check_encode_type(ord(lst_bytes[int_offset]))
        self._value = ''
        int_newoffset = int_offset + 1
        obj_length = IntEncoder()
        int_newoffset = obj_length.fromBytes(lst_bytes, int_newoffset)
        self._value = lst_bytes[int_newoffset + 1:int_newoffset + (2*obj_length.value):2]
        return int_newoffset + (2*obj_length.value)

    def toBytes(self):
        if self._value is None:
            return chr(self.EncodeType) + IntEncoder().toBytes()
        lst_bt = chr(self.EncodeType)
        obj_length = IntEncoder()
        obj_length.setValue(len(self._value))
        lst_bt += obj_length.toBytes()
        lst_bt += chr(DataType.Byte) + chr(DataType.Byte).join(self._value)
        return lst_bt

    def calcSize(self, lst_bytes, int_offset):
        int_new_offset = int_offset + 1
        obj_length = IntEncoder()
        int_new_offset = obj_length.fromBytes(lst_bytes, int_new_offset)
        return int_new_offset + (2*obj_length.value)

    @property
    def hexStr(self):
        """
        get the value of the bit field

        :return: a hex string
        :rtype: str
        """
        return ''.join('{0:02X}'.format(c) for c in self._value)
