from .baseencoder import BaseEncoder
from ..utils import DataType, BitField16
import struct

CharStruct = struct.Struct('<H')

class CharEncoder(BaseEncoder):

    EncodeType = DataType.Byte

    def fromBytes(self, lst_bytes, int_offset):
        self._value = unichr(CharStruct.unpack(lst_bytes[int_offset:int_offset + 2])[0])
        return int_offset + 2

    def toBytes(self):
        if self._value is None:
            return '\x00\x00'
        return CharStruct.pack(ord(self._value))

    def calcSize(self, lst_bytes, int_offset):
        return int_offset + 2