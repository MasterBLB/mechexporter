from .intencoder import IntEncoder
from .byteencoder import ByteEncoder
from .bytearrayencoder import ByteArrayEncoder
from .longencoder import LongEncoder
from .stringencoder import StringEncoder
from .floatencoder import FloatEncoder
from .booleanencoder import BooleanEncoder
from .baseencoder import BaseEncoder


def encoder_factory(lst_bytes, int_offset):
    """

    :param lst_bytes:
    :type lst_bytes:
    :param int_offset:
    :type int_offset:
    :return:
    :rtype: (BaseEncoder, int)
    """

    lst_encoders = [IntEncoder, ByteEncoder, ByteArrayEncoder, LongEncoder,
                    StringEncoder, FloatEncoder, BooleanEncoder]

    int_datatype = ord(lst_bytes[int_offset])

    for encoder in lst_encoders:
        if encoder.EncodeType == int_datatype:
            obj_encoder = encoder()
            int_offset = obj_encoder.fromBytes(lst_bytes, int_offset)
            return [obj_encoder, int_offset]
    raise Exception('No Encoder found for type: 0x{0:02X} at position 0x{1:02X}'.format(int_datatype, int_offset))

def encoder_size_factory(lst_bytes, int_offset):
    """

    :param lst_bytes:
    :type lst_bytes:
    :param int_offset:
    :type int_offset:
    :return:
    :rtype: (BaseEncoder, int)
    """

    lst_encoders = [IntEncoder, ByteEncoder, ByteArrayEncoder, LongEncoder,
                    StringEncoder, FloatEncoder, BooleanEncoder]

    int_datatype = lst_bytes[int_offset]

    for encoder in lst_encoders:
        if encoder.EncodeType == int_datatype:
            obj_encoder = encoder()
            int_offset = obj_encoder.calcSize(lst_bytes, int_offset)
            return [obj_encoder, int_offset]
    raise Exception('No Encoder found for type: 0x{0:02X} at position 0x{1:02X}'.format(int_datatype, int_offset))