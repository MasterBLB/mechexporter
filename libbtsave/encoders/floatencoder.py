from .baseencoder import BaseEncoder
import struct
from ..utils import DataType, convert_to_string, convert_to_bytes


class FloatEncoder(BaseEncoder):
    EncodeType = DataType.Float

    def fromBytes(self, lst_bytes, int_offset):
        self.check_encode_type(ord(lst_bytes[int_offset]))
        self._value = 0.0
        int_new_offset = int_offset + 5
        self._value = struct.unpack('<f', lst_bytes[int_offset+1:int_offset+5])[0]
        return int_new_offset

    def toBytes(self):
        if self._value is None:
            return chr(DataType.Float) + struct.pack('<f', 0.0)
        return chr(DataType.Float) + struct.pack('<f', self._value)

    def calcSize(self, lst_bytes, int_offset):
        return int_offset + 5