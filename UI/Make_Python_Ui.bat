@echo off
del ..\editor.py
del ..\mechnameDialog.py
del ..\arrayeditorui.py
del ..\images_rc.py
del ..\stylesheets_rc.py
del ..\descriptioneditdialog.py
del ..\hardpointeditorui.py
del ..\actuatorDialog.py
del ..\AssemblyVariantDialog.py
del ..\ChassisDefaultsDialog.py
pyside-uic.exe -o ..\hardpointeditorui.py .\hardpointeditorui.ui
pyside-uic.exe -o ..\mechnameDialog.py .\mechnameDialog.ui
pyside-uic.exe -o ..\descriptioneditdialog.py .\descriptioneditdialog.ui
pyside-uic.exe -o ..\editor.py .\editor.ui
pyside-uic.exe -o ..\arrayeditorui.py .\arrayeditorui.ui
pyside-uic.exe -o ..\actuatorDialog.py .\actuatorDialog.ui
pyside-uic.exe -o ..\AssemblyVariantDialog.py .\AssemblyVariantDialog.ui
pyside-uic.exe -o ..\ChassisDefaultsDialog.py .\ChassisDefaultsDialog.ui
pyside-rcc.exe -o ..\images_rc.py .\images.qrc
pyside-rcc.exe -o ..\stylesheets_rc.py .\stylesheets.qrc
set /p x="Build complete! Press Enter to finish."