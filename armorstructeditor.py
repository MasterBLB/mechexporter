from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from arrayeditorui import Ui_Form_arrayEditor
from models import ArmorStructTableModel


class ArmorStructEditor(QDialog, Ui_Form_arrayEditor):

    """
    the generic array tab handler

    """

    def __init__(self, lst_data, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(ArmorStructEditor, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.setWindowTitle("Armor & Internal Structure Editor")
        self.setMinimumWidth(750)
        self.SaveOnExit = False
        self.Data = []
        self.pushButton_Done.pressed.connect(self.donePressed)

        self.dataModel = ArmorStructTableModel(lst_data)

        # set up the inventory table
        self.tableView.setModel(self.dataModel)
        self.tableView.verticalHeader().show()
        self.tableView.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        self.tableView.setSelectionMode(QAbstractItemView.SingleSelection)

    def donePressed(self):
        self.Data = self.dataModel.inventoryRecords
        self.SaveOnExit = True
        self.close()