from _winreg import ConnectRegistry, OpenKey, EnumValue, HKEY_CURRENT_USER, QueryInfoKey
from base64 import standard_b64decode
from libbtsave import ProtobufMessage, FlattenedArray, NestedClass

aReg = ConnectRegistry(None,HKEY_CURRENT_USER)

aKey = OpenKey(aReg, "Software\Harebrained Schemes\BATTLETECH")
valLength = QueryInfoKey(aKey)
for i in range(valLength[1]):
    try:
        val = EnumValue(aKey, i)
        if 'CachedSettings_' in val[0]:
            data = val[1]
            y = ProtobufMessage()
            y.fromBytes(standard_b64decode(data), 0)
            z = y.get_field(28).data
            a = ProtobufMessage()
            a.fromBytes(z, 0)
            b = ProtobufMessage()
            b.fromBytes(a.get_field(2).data, 0)
            f = FlattenedArray()
            f.fromBytes(b.get_field(1).data, 0)
            count = 0
            for element in f.allElements:
                c = ProtobufMessage()
                c.fromBytes(element, 0)
                x = open('mech{0}.json'.format(count), 'wb')
                x.write(c.get_field(1).data)
                x.close()
                count += 1


    except EnvironmentError:
        break