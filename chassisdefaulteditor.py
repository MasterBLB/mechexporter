from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from arrayeditorui import Ui_Form_arrayEditor
from models import ChassisDefaultTableModel
from chassisdefaultmodify import ChassisDefaultsModify
from libbtmechporter import ChassisDefault

class ChassisDefaultEditor(QDialog, Ui_Form_arrayEditor):

    """
    the generic array tab handler

    """

    def __init__(self, lst_data, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(ChassisDefaultEditor, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.SaveOnExit = False
        self.Data = []
        self.pushButton_Done.pressed.connect(self.donePressed)

        self.dataModel = ChassisDefaultTableModel(lst_data)

        # set up the inventory table
        self.tableView.setModel(self.dataModel)
        self.tableView.verticalHeader().show()
        self.tableView.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)
        self.tableView.setSelectionMode(QAbstractItemView.SingleSelection)

        self.tableView.setContextMenuPolicy(Qt.ActionsContextMenu)

        # add actions to the right-click menu
        actionAdd = QAction(self.tableView)
        actionAdd.setText('Add Chassis Default')
        actionAdd.triggered.connect(self.newChassisDefault)
        self.tableView.addAction(actionAdd)

        actionEdit = QAction(self.tableView)
        actionEdit.setText('Edit Chassis Default')
        actionEdit.triggered.connect(self.editChassisDefault)
        self.tableView.addAction(actionEdit)

        actionRemove = QAction(self.tableView)
        actionRemove.setText('Remove Chassis Default')
        actionRemove.triggered.connect(self.removeChassisDefault)
        self.tableView.addAction(actionRemove)


    def donePressed(self):
        self.Data = self.dataModel.getDefaults()
        self.SaveOnExit = True
        self.close()


    def newChassisDefault(self):

        dialog = ChassisDefaultsModify()
        dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        dialog.setWindowIcon(self.windowIcon())
        dialog.comboBox.addItems(self.dataModel.ChassisLocations)
        result = dialog.exec_()
        if result == QDialog.Accepted:
            data = ChassisDefault()
            data.Location = dialog.comboBox.currentText()
            data.Id = dialog.lineEdit_Id.text()
            data.Category = dialog.lineEdit_Cat.text()
            data.Type = dialog.lineEdit_Type.text()
            self.dataModel.addRecord(data)

    def editChassisDefault(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(item)
        if len(lst_idx) > 0:
            initVals = self.dataModel.getRecord(lst_idx[0])

            dialog = ChassisDefaultsModify()
            dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            dialog.setWindowIcon(self.windowIcon())
            dialog.comboBox.addItems(self.dataModel.ChassisLocations)
            dialog.comboBox.setCurrentIndex(self.dataModel.ChassisLocations.index(initVals.Location))
            dialog.lineEdit_Type.setText(initVals.Type)
            dialog.lineEdit_Cat.setText(initVals.Category)
            dialog.lineEdit_Id.setText(initVals.Id)
            result = dialog.exec_()
            if result == QDialog.Accepted:
                data = ChassisDefault()
                data.Location = dialog.comboBox.currentText()
                data.Id = dialog.lineEdit_Id.text()
                data.Category = dialog.lineEdit_Cat.text()
                data.Type = dialog.lineEdit_Type.text()
                self.dataModel.updateRecord(lst_idx[0], data)

    def removeChassisDefault(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.dataModel.deleteRecord(lst_idx[0])
