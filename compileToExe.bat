@echo off
rmdir /Q /S Compiled
rmdir /Q /S dist
pyinstaller BattleTechMechporter.spec
rmdir /Q /S build
rename dist Compiled
set /p x="Build complete! Press Enter to finish."